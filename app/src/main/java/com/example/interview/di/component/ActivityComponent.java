package com.example.interview.di.component;


import com.example.interview.di.PerActivity;
import com.example.interview.di.module.ActivityModule;
import com.example.interview.ui.activities.MainActivity;
import com.example.interview.ui.activities.MeetDetailActivity;

import dagger.Component;


@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(MainActivity activity);

    void inject(MeetDetailActivity detailActivity);

}

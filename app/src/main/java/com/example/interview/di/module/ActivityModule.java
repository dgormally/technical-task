package com.example.interview.di.module;

import android.content.Context;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.interview.di.ActivityContext;
import com.example.interview.di.PerActivity;
import com.example.interview.model.Meeting;
import com.example.interview.model.User;
import com.example.interview.ui.Adapters.MeetAdapter;
import com.example.interview.ui.Adapters.UsersMeetAdapter;
import com.example.interview.utils.Navigator;

import java.util.ArrayList;

import dagger.Module;
import dagger.Provides;
import io.reactivex.disposables.CompositeDisposable;

@Module
public class ActivityModule {

    private AppCompatActivity mActivity;

    public ActivityModule(AppCompatActivity activity) {
        this.mActivity = activity;
    }

    @Provides
    @ActivityContext
    Context provideContext() {
        return mActivity;
    }

    @Provides
    AppCompatActivity provideActivity() {
        return mActivity;
    }

    @Provides
    CompositeDisposable provideCompositeDiaposable() {
        return new CompositeDisposable();
    }



    @Provides
    LinearLayoutManager provideLinearLayoutManager(AppCompatActivity activity) {
        return new LinearLayoutManager(activity);
    }


    @Provides
    @PerActivity
    MeetAdapter provideMeetAdapter() {
        return new MeetAdapter(new ArrayList<Meeting>());
    }

    @Provides
    @PerActivity
    UsersMeetAdapter provideUsersMeetAdapter() {
        return new UsersMeetAdapter(new ArrayList<User>());
    }


    @Provides
    Navigator provideNavigator() {
        return new Navigator();
    }

}

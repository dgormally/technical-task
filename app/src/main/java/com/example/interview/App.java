package com.example.interview;

import android.app.Application;
import android.content.Context;
import com.example.interview.di.component.ApplicationComponent;
import com.example.interview.di.component.DaggerApplicationComponent;
import com.example.interview.di.module.ApplicationModule;

public class App extends Application {

    private static Context context;

    private ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();
        context=this;
        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this)).build();
        mApplicationComponent.inject(this);
    }

    public static Context getmContext() {
        return context;
    }
    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }


}

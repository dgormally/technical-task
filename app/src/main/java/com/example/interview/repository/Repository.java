package com.example.interview.repository;

import android.content.Context;
import android.util.Log;

import com.example.interview.model.MeetsResponse;
import com.example.interview.model.MockedData;
import com.example.interview.model.User;
import com.google.gson.Gson;

import io.reactivex.Observable;
import io.reactivex.Single;

public class Repository {

    private static final String TAG = "Repository";
    private static Repository instance;
    private static MockedData mockedData;
    Context mContext;

    public Repository(Context context) {
        mContext = context;
        mockedData = new MockedData();
    }

    public static synchronized Repository getInstance(Context context) {
        if (instance == null) {
            instance = new Repository(context);
        }
        return instance;
    }

    public Observable<User> getCurrentUser() {
        return Observable.just(mockedData.getCurrentUser());
    }


    public Observable<MeetsResponse> getMeetings() {
        return Observable.just(mockedData.getMeetingsForUser());
    }

}

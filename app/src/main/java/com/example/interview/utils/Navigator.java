package com.example.interview.utils;

import android.content.Context;
import androidx.annotation.NonNull;
import com.example.interview.Base.BaseActivity;
import com.example.interview.R;
import com.example.interview.model.Meeting;
import com.example.interview.ui.activities.MeetDetailActivity;

public class Navigator {

    public Navigator() {
        //Required empty constructor
    }

    public void navigateToDetailsView(@NonNull Context context, Meeting meeting) {
        context.startActivity(MeetDetailActivity.getCallingIntent(context, meeting));
        ActivityTransitions.setSlideExitToRightAnimation(context);
    }


    private static class ActivityTransitions {

        static void setFadeInOutAnimation(@NonNull Context context) {
            if (context instanceof BaseActivity) {
                ((BaseActivity) context).overridePendingTransition(
                        R.anim.anim_fade_in, R.anim.anim_fade_out);
            }
        }

        static void setSlideExitToRightAnimation(@NonNull Context context) {
            if (context instanceof BaseActivity) {
                ((BaseActivity) context).overridePendingTransition(
                        R.anim.anim_no_change, R.anim.anim_slide_exit_to_right);
            }
        }
    }
}

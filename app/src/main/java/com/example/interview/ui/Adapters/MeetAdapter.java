package com.example.interview.ui.Adapters;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.interview.R;
import com.example.interview.model.Meeting;

import java.util.ArrayList;

public class MeetAdapter extends RecyclerView.Adapter<MeetAdapter.MeetVHAdapter> {

    private static final String TAG = "MeetAdapter";

    private ArrayList<Meeting> meetings;
    private Action action;


    public void addItems(ArrayList<Meeting> meetings) {
        this.meetings.addAll(meetings);
        notifyDataSetChanged();
    }

    public interface Action {
        void clickOnItem(Meeting meeting);
    }


    public MeetAdapter(ArrayList<Meeting> meetings) {
        this.meetings = meetings;

    }

    public void setAction(Action action) {
        this.action = action;
    }

    @NonNull
    @Override
    public MeetVHAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        return new MeetVHAdapter(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_meet, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MeetVHAdapter meetVHAdapter, int i) {
        meetVHAdapter.tvMeetDate.setText(meetings.get(i).getDate());
        meetVHAdapter.tvMeetLocation.setText(meetings.get(i).getLocation());
        meetVHAdapter.tvMeetName.setText(meetings.get(i).getName());
        meetVHAdapter.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (action != null) action.clickOnItem(meetings.get(i));
            }
        });

    }

    @Override
    public int getItemCount() {
        return meetings.size();
    }

    public class MeetVHAdapter extends RecyclerView.ViewHolder {

        TextView tvMeetDate, tvMeetLocation, tvMeetName;

        public MeetVHAdapter(@NonNull View itemView) {
            super(itemView);
            tvMeetDate = itemView.findViewById(R.id.tvMeetDate);
            tvMeetLocation = itemView.findViewById(R.id.tvMeetLocation);
            tvMeetName = itemView.findViewById(R.id.tvMeetName);
        }
    }
}

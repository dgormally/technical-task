package com.example.interview.ui.activities;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;

import com.example.interview.Base.BaseActivity;
import com.example.interview.Navigators.BaseNavigator;
import com.example.interview.R;
import com.example.interview.databinding.ActivityMainBinding;
import com.example.interview.di.component.ActivityComponent;
import com.example.interview.modelview.HomeMV;
import com.example.interview.ui.Adapters.MeetAdapter;
import com.example.interview.utils.Navigator;
import com.squareup.picasso.Picasso;

import javax.inject.Inject;

public class MainActivity extends BaseActivity<ActivityMainBinding, HomeMV> implements BaseNavigator {

    private static final String TAG = "MainActivity";

    @Inject
    Navigator navigator;

    @Inject
    MeetAdapter meetAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;

    HomeMV viewModel;

    ActivityMainBinding viewDataBinding;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getViewModel();
        viewModel.setNavigator(this);

        viewDataBinding = getViewDataBinding();
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);

        }

        viewDataBinding.rvMeets.setLayoutManager(mLayoutManager);
        viewDataBinding.rvMeets.setAdapter(meetAdapter);
        Log.d(TAG, "onCreate:meetAdapter "+meetAdapter);
        Log.d(TAG, "onCreate:mLayoutManager "+mLayoutManager);

        viewModel.getUser().observe(this, user -> {
            Log.d(TAG, "onCreate:getImageUrl "+user.getImageUrl());
            if (user != null) {
                viewDataBinding.edUsername.setText(user.getName());
                Picasso.get().load(user.getImageUrl()).into(viewDataBinding.ivUserImage);
            }
        });

        viewModel.getMeetingList().observe(this, meetings -> {
            if (meetings == null || meetings.isEmpty()) return;

              meetAdapter.addItems(meetings);
              meetAdapter.setAction(meeting -> {
                 navigator.navigateToDetailsView(this, meeting);
              });
        });

        viewModel.getCurrentUser();
        viewModel.getMeetingsForUser();

    }

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public HomeMV getViewModel() {
        return ViewModelProviders.of(this).get(HomeMV.class);
    }

    @Override
    public void handleError(Throwable throwable) {

        //To display in the dialog message
        throwable.getLocalizedMessage();
    }

    @Override
    public void handleSuccess(String success) {}


    @Override
    public int getLayoutId() {
        return R.layout.activity_main;
    }
}

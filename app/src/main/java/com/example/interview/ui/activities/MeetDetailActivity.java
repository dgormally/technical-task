package com.example.interview.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import com.example.interview.Base.BaseActivity;
import com.example.interview.Navigators.BaseNavigator;
import com.example.interview.R;
import com.example.interview.databinding.ActivityMeetDetailBinding;
import com.example.interview.di.component.ActivityComponent;
import com.example.interview.model.Meeting;
import com.example.interview.modelview.MeetDetailMV;
import com.example.interview.ui.Adapters.UsersMeetAdapter;

import javax.inject.Inject;

public class MeetDetailActivity extends BaseActivity<ActivityMeetDetailBinding, MeetDetailMV> implements BaseNavigator {

    public static final String KEY_MEET = "meet";

    public static Intent getCallingIntent(Context context, Meeting meeting) {
        Intent intent = new Intent(context,MeetDetailActivity.class);
        intent.putExtra(MeetDetailActivity.KEY_MEET, meeting);
        return intent;
    }

    MeetDetailMV viewModel;
    ActivityMeetDetailBinding viewDataBinding;

    @Inject
    UsersMeetAdapter usersMeetAdapter;

    @Inject
    LinearLayoutManager mLayoutManager;



    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return super.onSupportNavigateUp();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        viewModel = getViewModel();
        viewDataBinding = getViewDataBinding();
        ActivityComponent component = getActivityComponent();
        if (component != null) {
            component.inject(this);

        }



        viewModel.setNavigator(this);
        if (getSupportActionBar() != null)
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        final Meeting meeting = getIntent().getParcelableExtra(KEY_MEET);
        if (meeting != null) {

            viewDataBinding.tvMeetLocation.setText(meeting.getLocation());
            viewDataBinding.tvMeetName.setText(meeting.getName());
            viewDataBinding.tvMeetTime.setText(meeting.getDate());
            viewDataBinding.rvUsers.setLayoutManager(mLayoutManager);
            viewDataBinding.rvUsers.setAdapter(usersMeetAdapter);
            usersMeetAdapter.addItems(meeting.getParticipants());
        }
    }

    @Override
    public int getBindingVariable() {
        return 0;
    }

    @Override
    public MeetDetailMV getViewModel() {
        return ViewModelProviders.of(this).get(MeetDetailMV.class);
    }

    @Override
    public void handleError(Throwable throwable) {

        //This will get displayed in the dialog message
         throwable.getLocalizedMessage();

    }

    @Override
    public void handleSuccess(String success) {

    }

    @Override
    public int getLayoutId() {
        return R.layout.activity_meet_detail;
    }


}

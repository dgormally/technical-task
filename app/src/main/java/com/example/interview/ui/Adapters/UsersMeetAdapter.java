package com.example.interview.ui.Adapters;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.interview.R;
import com.example.interview.model.User;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class UsersMeetAdapter extends RecyclerView.Adapter<UsersMeetAdapter.MeetVHAdapter> {

    private static final String TAG = "UsersMeetAdapter";
    ArrayList<User>users;

    public UsersMeetAdapter(ArrayList<User> users) {
        this.users = users;
    }

    @NonNull
    @Override
    public MeetVHAdapter onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        return new MeetVHAdapter(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_user_meet,viewGroup,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MeetVHAdapter meetVHAdapter, int i) {

        meetVHAdapter.edUsername.setText(users.get(i).getName());
        Log.d(TAG, "onBindViewHolder: "+users.get(i).getImageUrl());
        Picasso.get().load(users.get(i).getImageUrl()).into(meetVHAdapter.ivUserImage);
    }

    @Override
    public int getItemCount() {
        return users.size();
    }

    public void addItems(ArrayList<User> participants) {
        users.addAll(participants);
        notifyDataSetChanged();
    }

    public  class MeetVHAdapter extends RecyclerView.ViewHolder{

        ImageView ivUserImage;
        TextView edUsername;

        public MeetVHAdapter(@NonNull View itemView) {
            super(itemView);
            ivUserImage= itemView.findViewById(R.id.ivUserImage);
            edUsername= itemView.findViewById(R.id.edUsername);

        }
    }
}

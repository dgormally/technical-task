package com.example.interview.modelview;

import android.app.Application;
import android.content.Context;

import androidx.annotation.NonNull;
import androidx.lifecycle.MutableLiveData;

import com.example.interview.Base.BaseViewModel;
import com.example.interview.Navigators.BaseNavigator;
import com.example.interview.model.Meeting;
import com.example.interview.model.User;

import java.util.ArrayList;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;

public class HomeMV extends BaseViewModel<BaseNavigator> {

    Context context;
    private MutableLiveData<User> currentUser = new MutableLiveData<>();
    private MutableLiveData<ArrayList<Meeting>> meetingList = new MutableLiveData<>();


    public HomeMV(@NonNull Application application) {
        super(application);
        this.context = context;

    }

    public MutableLiveData<ArrayList<Meeting>> getMeetingList() {
        return meetingList;
    }

    public MutableLiveData<User> getUser() {
        return currentUser;
    }


    public void getCurrentUser() {
        setIsLoading(true);
        getCompositeDisposable().add(
                getDataManager().getCurrentUser()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> {
                            currentUser.setValue(response);
                            setIsLoading(false);
                            getNavigator().handleSuccess("Success");
                        }, throwable -> {
                            showErrorLog("error " + throwable.getMessage());
                            getNavigator().handleError(getErrorThrowable(context, throwable));
                            setIsLoading(false);
                        })
        );
    }

    public void getMeetingsForUser() {
        setIsLoading(true);
        getCompositeDisposable().add(
                getDataManager().getMeetings()
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe(response -> {
                            meetingList.setValue(response.getMeetings());
                            getNavigator().handleSuccess("Success");
                            setIsLoading(false);
                        }, throwable -> {
                            showErrorLog("error " + throwable.getMessage());
                            getNavigator().handleError(getErrorThrowable(context, throwable));
                            setIsLoading(false);
                        })
        );
    }
}

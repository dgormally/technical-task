package com.example.interview.model;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class ErrorResult {

    @SerializedName("errors")
    private ErrorMessageResult errorsMessage;

    public ErrorResult() {
    }

    public String getMessage() {
        if (errorsMessage != null)
            return errorsMessage.getMessage();
        return null;
    }

    public class ErrorMessageResult {

        @SerializedName("email")
        private List<String> email;
        @SerializedName("name")
        private List<String> name;
        @SerializedName("password")
        private List<String> password;
        @SerializedName("entries")
        private List<String> entries;

        public ErrorMessageResult() {

        }

        private StringBuilder getMessage(StringBuilder stringBuilder, List<String> list) {
            if (list != null && !list.isEmpty()) {
                for (String message : list) {
                    stringBuilder.append(message);
                    stringBuilder.append("\n");
                }
            }
            return stringBuilder;
        }

        private List<List<String>> getItems() {
            List<List<String>> items = new ArrayList<>();
            items.add(email);
            items.add(name);
            items.add(password);
            items.add(entries);
            return items;
        }

        public String getMessage() {
            StringBuilder stringBuilder = new StringBuilder();
            for (List<String> list : getItems()) {
                stringBuilder = getMessage(stringBuilder, list);
            }
            return stringBuilder.toString().trim();
        }
    }
}


package com.example.interview.model;

import com.google.gson.annotations.SerializedName;

public class ErrorResultAlternative {

    @SerializedName("error")
    private ErrorResult.ErrorMessageResult errorsMessage;

    public ErrorResultAlternative() {}

    public String getMessage() {
        if (errorsMessage != null)
            return errorsMessage.getMessage();
        return null;
    }
}


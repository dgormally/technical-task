package com.example.interview.model;


import java.util.ArrayList;
import java.util.Calendar;

/**
 * Mocked data for the test, assume this to be your data repository
 */
public class MockedData {

    private static final String TAG = "MockedData";


    /**
     * Synchronously retrieves the "current user" (assume you are logged in somehow)
     * @return A string JSON representation of the current user
     */
    public User getCurrentUser() {
        User user = new User();
        user.setId(0);
        user.setImageUrl("https://images.vexels.com/media/users/3/145908/preview2/52eabf633ca6414e60a7677b0b917d92-male-avatar-maker.jpg");
        user.setName("Me");
        return user;
    }

    /**
     * Synchronously retrieves a user
     * @param id the ID of the user
     * @return @return A string JSON representation of the user if the user exists,
     * and the currrent user can access the profile, null otherwise
     */
    public User getUser(int id) {
        User user = new User();
        user.setId(id);
        user.setImageUrl("https://images.vexels.com/media/users/3/145908/preview2/52eabf633ca6414e60a7677b0b917d92-male-avatar-maker.jpg");
        user.setName("Someone else " + id);
        return user;
    }


    public MeetsResponse getMeetingsForUser() {
        MeetsResponse response = new MeetsResponse();
        ArrayList<Meeting> meetings = new ArrayList<>();
        for (int i = 1; i < 10; i++) {
            meetings.add(createFakeMeet(i));
        }
        response.setMeetings(meetings);
        return response;
    }

    public Meeting createFakeMeet(int meetId) {
        ArrayList<User> participants = new ArrayList<>();
        for (int i = 1; i < 10 ; i++) {
            participants.add(getUser(i));
        }
        Meeting meeting = new Meeting();
        meeting.setDate( Calendar.getInstance().getTime().toString());
        meeting.setLocation("Uk");
        meeting.setName("Meet "+meetId);
        meeting.setParticipants(participants);
        return meeting;
    }

}

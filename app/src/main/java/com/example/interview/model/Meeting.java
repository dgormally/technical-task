package com.example.interview.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.SingleObserver;

public class Meeting extends Single<Meeting> implements Parcelable {

    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("date")
    @Expose
    private String date;

    @SerializedName("location")
    @Expose
    private String location;

    @SerializedName("participants")
    @Expose
    private ArrayList<User> participants;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public ArrayList<User> getParticipants() {
        return participants;
    }

    public void setParticipants(ArrayList<User> participants) {
        this.participants = participants;
    }

    @Override
    protected void subscribeActual(SingleObserver<? super Meeting> observer) {

    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.name);
        dest.writeString(this.date);
        dest.writeString(this.location);
        dest.writeList(this.participants);
    }

    public Meeting() {
    }

    protected Meeting(Parcel in) {
        this.name = in.readString();
        this.date = in.readString();
        this.location = in.readString();
        this.participants = new ArrayList<User>();
        in.readList(this.participants, User.class.getClassLoader());
    }

    public static final Creator<Meeting> CREATOR = new Creator<Meeting>() {
        @Override
        public Meeting createFromParcel(Parcel source) {
            return new Meeting(source);
        }

        @Override
        public Meeting[] newArray(int size) {
            return new Meeting[size];
        }
    };

    @Override
    public String toString() {
        return "Meeting{" +
                "name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", location='" + location + '\'' +
                ", participants=" + participants +
                '}';
    }
}

package com.example.interview.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

import io.reactivex.Single;
import io.reactivex.SingleObserver;

public class MeetsResponse extends Single<MeetsResponse> {

    @SerializedName("meets")
    @Expose
    ArrayList<Meeting> meetings;

    public ArrayList<Meeting> getMeetings() {
        return meetings;
    }

    public void setMeetings(ArrayList<Meeting> meetings) {
        this.meetings = meetings;
    }

    @Override
    protected void subscribeActual(SingleObserver<? super MeetsResponse> observer) {}

    @Override
    public String toString() {
        return "MeetsResponse{" +
                "meetings=" + meetings +
                '}';
    }
}

/*
 *  Copyright (C) 2017 MINDORKS NEXTGEN PRIVATE LIMITED
 *
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *
 *      https://mindorks.com/license/apache-v2
 *
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License
 */

package com.example.interview.Base;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.MutableLiveData;

import com.example.interview.R;
import com.example.interview.model.ErrorResult;
import com.example.interview.model.ErrorResultAlternative;
import com.example.interview.repository.Repository;
import com.example.interview.utils.AppConstants;
import com.example.interview.utils.NetworkUtils;
import com.google.gson.Gson;
import com.google.gson.JsonParseException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.ref.WeakReference;
import java.net.UnknownHostException;

import io.reactivex.disposables.CompositeDisposable;
import okhttp3.ResponseBody;
import retrofit2.HttpException;
import retrofit2.Response;


public abstract class BaseViewModel<N> extends AndroidViewModel {

    private static final String TAG = "BaseViewModel";
    private final Repository mDataManager;
    private final MutableLiveData<Boolean> mIsLoading = new MutableLiveData<Boolean>();
    private CompositeDisposable mCompositeDisposable = new CompositeDisposable();
    private WeakReference<N> mNavigator;


    public BaseViewModel(@NonNull Application application) {
        super(application);
        mDataManager = Repository.getInstance(application.getApplicationContext());
    }

    @Override
    protected void onCleared() {
        mCompositeDisposable.dispose();
        super.onCleared();
    }

    public CompositeDisposable getCompositeDisposable() {
        return mCompositeDisposable;
    }

    public Repository getDataManager() {
        return mDataManager;
    }

    public MutableLiveData<Boolean> getIsLoading() {
        return mIsLoading;
    }

    public void setIsLoading(boolean isLoading) {
        mIsLoading.setValue(isLoading);
    }


    public N getNavigator() {
        return mNavigator.get();
    }

    public void setNavigator(N navigator) {
        this.mNavigator = new WeakReference<>(navigator);
    }

    public boolean isNetworkConnected() {
        return NetworkUtils.isNetworkConnected(getApplication());
    }


    public void showErrorLog(String d) {
        Log.d(TAG, "showErrorLog: " + d);
    }

    @Nullable
    public Throwable getErrorThrowable(Context context, Throwable throwable) {
        ErrorResult errorResult = null;
        String errorMessage = null;
        if (throwable == null)
            return null;
        if (throwable instanceof FileNotFoundException)
            return throwable;
        if (throwable instanceof IOException)
            return new Throwable(context.getString(R.string.error_check_internet_connection));
        if (throwable instanceof UnknownHostException)
            return new Throwable(context.getString(R.string.error_check_internet_connection));

        if (throwable instanceof HttpException) {
            Response response = ((HttpException) throwable).response();
            if (response.errorBody() != null) {
                Log.d(TAG, "Status code is: " + response.code());
                if (response.code() == 404) {
                    return null;
                }
                if (response.code() == 401) {
                    return null;
                }
                ResponseBody body = response.errorBody();
                Gson gson = new Gson();
                try {
                    String bodyStr = body.string();
                    if (bodyStr != null) {
                        errorResult = gson.fromJson(bodyStr, ErrorResult.class);
                    }
                    if (errorResult != null) {
                        errorMessage = errorResult.getMessage();
                    }
                    if (errorMessage == null) {
                        errorMessage = getAlternativeErrorParse(body, gson);
                    }
                } catch (IOException | JsonParseException e1) {
                    ///Log.e(Presenter.class.getSimpleName(), e1.getLocalizedMessage());
                    errorMessage = getAlternativeErrorParse(body, gson);
                }
            }
        }
        if (!TextUtils.isEmpty(errorMessage))
            return new Throwable(errorMessage);
        return null;
    }

    private String getAlternativeErrorParse(ResponseBody body, Gson gson) {
        try {
            String bodyStr = body.string();
            ErrorResultAlternative errorResult =
                    gson.fromJson(bodyStr, ErrorResultAlternative.class);
            if (errorResult != null) {
                return errorResult.getMessage();
            }
        } catch (IOException | JsonParseException e2) {
        }
        return null;
    }
    public void showSuccessLog(String d) {
        Log.d(TAG, "showSuccessLog: " + d);
    }

    public boolean isSuccess(String status) {
        return status.equalsIgnoreCase(AppConstants.SUCCESS);
    }
}
